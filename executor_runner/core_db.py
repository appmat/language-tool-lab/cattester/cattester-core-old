import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import JSONB

Base = declarative_base()


class Theme(Base):
    __tablename__ = 'theme'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(200), unique=True, nullable=False)

    def __repr__(self) -> str:
        return 'Theme({}, {})'.format(self.id, self.name)


class Problem(Base):
    __tablename__ = 'problem'
    id = sa.Column(sa.Integer, primary_key=True)
    theme_id = sa.Column(sa.Integer, sa.ForeignKey('theme.id', ondelete='CASCADE'), nullable=False)
    title = sa.Column(sa.String(200), nullable=False)
    description = sa.Column(sa.Text, nullable=False)
    last_update_time = sa.Column(sa.DateTime, nullable=False)

    def __repr__(self) -> str:
        return 'Problem({}, {}, {}, {}, {})'.\
            format(self.id, self.theme_id, self.title, self.description, self.last_update_time)


class Submission(Base):
    __tablename__ = 'submission'
    id = sa.Column(sa.Integer, primary_key=True)
    problem_id = sa.Column(sa.Integer, sa.ForeignKey('problem.id', ondelete='CASCADE'), nullable=False)

    def __repr__(self) -> str:
        return 'Submission({}, {})'.format(self.id, self.problem_id)


class SubmissionFile(Base):
    __tablename__ = 'submission_file'
    id = sa.Column(sa.Integer, primary_key=True)
    submission_id = sa.Column(sa.Integer, sa.ForeignKey('submission.id', ondelete='CASCADE'), nullable=False)
    name = sa.Column(sa.Text, nullable=False)
    file = sa.Column(sa.Text, nullable=False)

    def __repr__(self) -> str:
        return 'SubmissionFile({}, {}, {}, {})'.format(self.id, self.submission_id, self.name, self.file)

    def to_executor(self):
        submission_file_object = {
            'name': self.name,
            'text': self.file
        }
        return submission_file_object


class Test(Base):
    __tablename__ = 'test'
    id = sa.Column(sa.Integer, primary_key=True)
    index = sa.Column(sa.Integer, nullable=False)
    problem_id = sa.Column(sa.Integer, sa.ForeignKey('problem.id', ondelete='CASCADE'), nullable=False)
    input = sa.Column(sa.Text)
    expected_output = sa.Column(sa.Text, nullable=False)
    public = sa.Column(sa.Boolean, default=False)

    def __repr__(self) -> str:
        return 'Test({}, {}, {}, {}, {}, {})'.\
            format(self.id, self.index, self.problem_id, self.input, self.expected_output, self.public)

    def to_executor(self):
        test_object = {
            'id': self.id,
            'input': self.input,
            'output': self.expected_output
        }
        return test_object


class TestResult(Base):
    __tablename__ = 'test_result'
    test_id = sa.Column(sa.Integer, sa.ForeignKey('test.id', ondelete='CASCADE'), primary_key=True)
    submission_id = sa.Column(sa.Integer, sa.ForeignKey('submission.id', ondelete='CASCADE'), primary_key=True)
    result_status = sa.Column(sa.String(20))
    date_time = sa.Column(sa.DateTime, nullable=False)
    duration = sa.Column(sa.Interval, nullable=False)
    error_output = sa.Column(sa.Text)
    error_title = sa.Column(sa.String(20))
    execution_status = sa.Column(sa.String(20))
    profile_statistics = sa.Column(JSONB)
    metrics = sa.Column(JSONB)

    def __repr__(self) -> str:
        return 'TestResult({}, {}, {}, {}, {}, {}, {}, {}, {}, {})'.\
            format(self.test_id, self.submission_id, self.result_status, self.date_time, self.duration,
                   self.error_output, self.error_title, self.execution_status, self.profile_statistics, self.metrics)
