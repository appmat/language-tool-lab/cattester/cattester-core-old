from executor import RequestExecutor
from queries import get_submission_files, get_tests
from executor_runner import Session


def run_executor(submission_id):
    session = Session()

    submission_files = []
    for file in get_submission_files(submission_id, session):
        submission_files.append(file.to_executor())

    tests = []
    for test in get_tests(submission_id, session):
        tests.append(test.to_executor())

    data = {'files': submission_files, 'tests': tests}
    executor = RequestExecutor('/execute')
    executor.run(data)

    session.close()
