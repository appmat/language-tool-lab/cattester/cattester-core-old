import requests
from executor_runner import executor_url


class RequestExecutor(object):
    def __init__(self, url):
        self.url = executor_url + url

    def get(self):
        return requests.get(self.url)

    def run(self, data):
        return requests.post(self.url, data=data)
