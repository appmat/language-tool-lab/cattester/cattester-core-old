"""create submission_file table

Revision ID: bbd0bd6c4fbe
Revises: ca52fa5c4ea9
Create Date: 2020-11-15 22:38:47.882231

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bbd0bd6c4fbe'
down_revision = 'ca52fa5c4ea9'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'submission_file',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('submission_id', sa.Integer, sa.ForeignKey('submission.id', ondelete='CASCADE'), nullable=False),
        sa.Column('name', sa.Text, nullable=False),
        sa.Column('file', sa.Text, nullable=False)
    )


def downgrade():
    op.drop_table('submission_file')
