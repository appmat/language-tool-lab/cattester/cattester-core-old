"""create theme table

Revision ID: 8d5bdbf906a4
Revises: 9abf58025df9
Create Date: 2020-12-13 22:41:50.142471

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8d5bdbf906a4'
down_revision = '9abf58025df9'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'theme',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('name', sa.String(200), unique=True, nullable=False)
    )

    op.add_column(
        'problem',
        sa.Column('theme_id', sa.Integer, sa.ForeignKey('theme.id', ondelete='CASCADE'), nullable=False)
    )


def downgrade():
    op.drop_table('theme')
    op.drop_column('problem', 'theme_id')
