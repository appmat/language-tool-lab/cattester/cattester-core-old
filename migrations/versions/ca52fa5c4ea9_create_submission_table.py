"""create submission table

Revision ID: ca52fa5c4ea9
Revises: ac16a004a4e3
Create Date: 2020-11-15 22:15:43.970674

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ca52fa5c4ea9'
down_revision = 'ac16a004a4e3'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'submission',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('problem_id', sa.Integer, sa.ForeignKey('problem.id', ondelete='CASCADE'), nullable=False)
    )


def downgrade():
    op.drop_table('submission')
