"""create test_result table

Revision ID: 9abf58025df9
Revises: affc5c43ad44
Create Date: 2020-11-15 23:43:44.571148

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB


# revision identifiers, used by Alembic.
revision = '9abf58025df9'
down_revision = 'affc5c43ad44'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'test_result',
        sa.Column('test_id', sa.Integer, sa.ForeignKey('test.id', ondelete='CASCADE'), primary_key=True),
        sa.Column('submission_id', sa.Integer, sa.ForeignKey('submission.id', ondelete='CASCADE'), primary_key=True),
        sa.Column('result_status', sa.String(20)),
        sa.Column('date_time', sa.DateTime, nullable=False),
        sa.Column('duration', sa.Interval, nullable=False),
        sa.Column('error_output', sa.Text),
        sa.Column('error_title', sa.String(20)),
        sa.Column('execution_status', sa.String(20)),
        sa.Column('profile_statistics', JSONB),
        sa.Column('metrics', JSONB)
    )


def downgrade():
    op.drop_table('test_result')
