CREATE TABLE group (
    id SERIAL PRIMARY KEY,
    group_name varchar(200) UNIQUE NOT NULL
);

CREATE TABLE user (
	id SERIAL PRIMARY KEY,
	email varchar(100) UNIQUE NOT NULL,
	pass_hash bytea NOT NULL,
	name varchar(50) NOT NULL,
	surname varchar(50) NOT NULL,
	patronymic varchar(50)
);

CREATE TABLE group_to_user (
    group_id int references group(id) ON DELETE CASCADE,
    user_id int references user(id) ON DELETE CASCADE,
    PRIMARY KEY (group_id, user_id)
);

CREATE TABLE theme (
    id SERIAL PRIMARY KEY,
    name varchar(200) UNIQUE NOT NULL,
    owner_id int references user(id)
);

CREATE TABLE group_to_theme (
    group_id int references group(id) ON DELETE CASCADE,
    theme_id int references theme(id) ON DELETE CASCADE,
    PRIMARY KEY (group_id, theme_id)
);

CREATE TABLE problem (
    id SERIAL PRIMARY KEY,
    creator_id int references user(id),
    theme_id int references theme(id) ON DELETE CASCADE,
    title varchar(200) NOT NULL,
    description text NOT NULL,
    last_update_time timestamp NOT NULL
);

CREATE TABLE problem_to_tag (
    problem_id int references problem(id) ON DELETE CASCADE,
    tag varchar(50) NOT NULL,
    PRIMARY KEY (problem_id)
);

CREATE TABLE environment (
    id SERIAL PRIMARY KEY,
    name varchar(100) UNIQUE NOT NULL
);

CREATE TABLE problem_to_environment (
    problem_id int references problem(id) ON DELETE CASCADE,
    environment_id int references environment(id),
    initial_code text,
    PRIMARY KEY (problem_id, environment_id)
);

CREATE TABLE submission (
    id SERIAL PRIMARY KEY,
    user_id int references user(id),
    problem_id int references problem(id) ON DELETE CASCADE,
    environment_id int references environment(id)
);

CREATE TABLE submission_file (
    id SERIAL PRIMARY KEY,
    submission_id int references submission(id) ON DELETE CASCADE,
    name text NOT NULL,
    file text NOT NULL
);

CREATE TABLE test (
    id SERIAL PRIMARY KEY,
    index int NOT NULL,
    problem_id int references problem(id) ON DELETE CASCADE,
    input text,
    expected_output text NOT NULL,
    public boolean DEFAULT false
);

CREATE TABLE test_result (
    test_id int references test(id) ON DELETE CASCADE,
    submission_id int references submission(id) ON DELETE CASCADE,
    result_status varchar(20),
    date_time timestamp NOT NULL,
    duration interval MINUTE TO SECOND NOT NULL,
    error_output text,
    error_title varchar(20),
    execution_status varchar(20),
    profile_statistics jsonb,
    metrics jsonb,
    PRIMARY KEY (test_id, submission_id)
);

CREATE TABLE test_to_environment (
    test_id int references test(id) ON DELETE CASCADE,
    environment_id int references environment(id),
    environment_params jsonb,
    time_limit int,    -- seconds
    memory_limit int,  -- MB
    default_launch_count int DEFAULT 1,
    PRIMARY KEY (test_id, environment_id)
);

/*
    здесь будут храниться человеко-читаемые описания метрик, 
    ключи которых будут использованы в json'ах в таблицах:
    * launch.profile_statistics
        [
            {'timestamp': 1234, 'metrics':[{'key':'cpu_usage', 'value': 0.1}]},
            {'timestamp': 1234, 'metrics':[{'key':'cpu_usage', 'value': 0.2}]}
        ]
    * launch.metrics
        [
            {'key': 'avg_cpu_usage', 'value': 0.15},
            {'key': 'max_memory_usage', 'value': 15000000}
        ]
*/
CREATE TABLE metric_dict (
    key text UNIQUE NOT NULL,
    description text NOT NULL
);