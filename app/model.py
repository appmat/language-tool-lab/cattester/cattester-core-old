from dataclasses import dataclass
from typing import Optional
from dataclasses_json import dataclass_json
from sqlalchemy.orm import registry
from app import sa
from sqlalchemy.dialects.postgresql import JSONB

mapper_registry = registry()

@mapper_registry.mapped
@dataclass_json()
@dataclass
class Theme:
    __table__ = sa.Table(
        'theme',
        mapper_registry.metadata,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(200), nullable=False)
    )

    name: str
    id: Optional[int] = None

@mapper_registry.mapped
@dataclass_json()
@dataclass
class Problem:
    __table__ = sa.Table(
        'problem',
        mapper_registry.metadata,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('title', sa.String(200), nullable=False),
        sa.Column('description', sa.Text, nullable=False),
        sa.Column('last_update_time', sa.DateTime, nullable=False),
        sa.Column('theme_id', sa.Integer, sa.ForeignKey('theme.id', ondelete='CASCADE'), nullable=False)
    )

    #id: int
    title: str
    description: str
    last_update_time: Optional[str] = None
    theme_id: Optional[int] = None
    id: Optional[int] = None

@mapper_registry.mapped
@dataclass_json()
@dataclass
class Submission:
    __table__ = sa.Table(
        'submission',
        mapper_registry.metadata,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('problem_id', sa.Integer, sa.ForeignKey('problem.id', ondelete='CASCADE'), nullable=False)
    )

    id: int
    problem_id: int

@mapper_registry.mapped
@dataclass_json()
@dataclass
class SubmissionFile:
    __table__ = sa.Table(
        'submission_file',
        mapper_registry.metadata,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('submission_id', sa.Integer, sa.ForeignKey('submission.id', ondelete='CASCADE'), nullable=False),
        sa.Column('name', sa.Text, unique=True, nullable=False),
        sa.Column('file', sa.Text, nullable=False)
    )

    name: str
    file: str
    id: Optional[int] = None
    submission_id: Optional[int] = None


@mapper_registry.mapped
@dataclass_json()
@dataclass
class Test:
    __table__ = sa.Table(
        'test',
        mapper_registry.metadata,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('index', sa.Integer, nullable=False),
        sa.Column('problem_id', sa.Integer, sa.ForeignKey('problem.id', ondelete='CASCADE'), nullable=False),
        sa.Column('input', sa.Text),
        sa.Column('expected_output', sa.Text, nullable=False),
        sa.Column('public', sa.Boolean, default=False)
    )

    id: int
    index: int
    problem_id: int
    input: str
    expected_output: str
    public: Optional[str] = None

@mapper_registry.mapped
@dataclass_json()
@dataclass
class TestResult:
    __table__ = sa.Table(
        'test_result',
        mapper_registry.metadata,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('test_id', sa.Integer, sa.ForeignKey('test.id', ondelete='CASCADE'), nullable=False),
        sa.Column('submission_id', sa.Integer, sa.ForeignKey('submission.id', ondelete='CASCADE'), nullable=False),
        sa.Column('result_status', sa.String(20)),
        sa.Column('date_time', sa.DateTime, nullable=False),
        sa.Column('duration', sa.Interval, nullable=False),
        sa.Column('error_output', sa.Text),
        sa.Column('error_title', sa.String(20)),
        sa.Column('execution_status', sa.String(20)),
        sa.Column('profile_statistics', JSONB),
        sa.Column('metrics', JSONB),
    )

    id: int
    test_id: int
    submission_id: int
    result_status: str
    date_time: str
    duration: str
    error_output: Optional[str] = None
    error_title: Optional[str] = None
    execution_status: Optional[str] = None
    profile_statistics: Optional[str] = None
    metrics: Optional[str] = None












